---
lang: en-US
title: "Foreign language. Grammar"
author: "Arseny Zorin"

fontsize: 12pt
linestretch: 1.5
documentclass: report
classoption: [notitlepage, onecolumn, openany]
geometry: [a4paper, bindingoffset=0mm, inner=30mm, outer=30mm, top=30mm, bottom=30mm] # See https://ctan.org/pkg/geometry for more options
header-includes:
  - \linepenalty=10
  - \interlinepenalty=0 # value of the penalty (node) added after each line of a paragraph.
  - \hyphenpenalty=50 # the penalty for line breaking at an automatically inserted hyphen
  - \exhyphenpenalty=50 # the penalty for line breaking at an explicit hyphen
  - \binoppenalty=700 # the penalty for breaking a line at a binary operator
  - \relpenalty=500 # the penalty for breaking a line at a relation
  - \clubpenalty=150 # extra penalty for breaking after first line of a paragraph
  - \widowpenalty=150 # extra penalty for breaking before last line of a paragraph
  - \displaywidowpenalty=50 # extra penalty for breaking before last line before a display math
  - \brokenpenalty=100 # extra penalty for page breaking after a hyphenated line
  - \predisplaypenalty=10000 # penalty for breaking before a display
  - \postdisplaypenalty=0 # penalty for breaking after a display
  - \floatingpenalty = 20000 # penalty for splitting an insertion (can only be split footnote in standard LaTeX)
  - \raggedbottom # or \flushbottom
  - \usepackage{float} # keep figures where there are in the text
  - \floatplacement{figure}{H} # keep figures where there are in the text
---

1. **__ (A)** Tap water here is not unpleasant (B) **to drink(C)** now they’ve removed (D) the fluoride.
2. **A lot (A)** of the senior staff are (B) **rightly (C)** concerned about (D) their pensions.
3. If the tickets had been cheaper (A), there (B) **would be(C)** more people at (D) the show.
4. Sports psychology (A) is **a (B)** science of behaviour **that applies(C)** to (D) exercise and sport participation.
5. Because the solar tiles were very **securely fastened (A)**, only a few (B) became detached(C) when the Space Shuttle reentered **Earth’s (D)** atmosphere.
6. He **said (A)** that his mobile phone had been (B) **turned off(C)** all (D) day.
7. **A lot of (A)** managers don’t **diligently (B)** prepare for(C) meetings at all (D).
36. The value of (A) the currency **having been fell** (B), made(C) foreign holidays more expensive (D).
8. Everyone knows that not allowing (A) **himself (B)** to show feelings of anger can(C) be very **harmful (D)**.
9. The committee sees (A) no (B) problem **with (C)** approving your application for (D) planning permission.
10. Parents (A) today consider the streets **are (B)** too(C) dangerous for children, so children are taken (D) from home to school and back.
