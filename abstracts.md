---
author: Arseny Zorin. 3560901/90601
title: Abstracts comparison
geometry: margin=1cm
papersize: a4
fontsize: 14pt
documentclass: extarticle
mainfont: Liberation Serif
---

There are two different abstracts in the suggested article.
First one is a concise summary of the paper by Julie Pham called
'Their War: The Perspective of the South Vietnamese Military in Their Own
Words'.
This abstract can be classified as a descriptive one.
It describes the work that has been done.
This work tries to change overall negative image of the Republic of Vietnam
Armed Forces.
The approach used in this study is to analyze qualitative interviews with 40
RVNAF veterans now living in San Jose, Sacramento and Seattle.
It is believed that this study will contribute to future research on similar
topics.

Another one abstract is an informative.
It is a summary of the 'Quantifying the Mechanics of a Laryngoscopy' essay.
It has a description of the work relevance which is shown by the need
for improved training methods.
It also has a delineation of Laryngoscopy procedure in common.
This abstract shows previous development in this field - it describes some
instrumented laryngoscop.
The goal of this study is to use comparisons between expert and novice users 
to identify the critical skill components necessary for patients.
This study helps to develop training mannequin for providing training feedback
for novice users.


