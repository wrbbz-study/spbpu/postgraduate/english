---
lang: en-US
title: "Foreign language. Prospects of my scientific career."
author: "Arseny Zorin"

fontsize: 12pt
linestretch: 1.5
documentclass: article
classoption: [notitlepage, onecolumn, openany]
geometry: [a4paper, bindingoffset=0mm, inner=30mm, outer=30mm, top=30mm, bottom=30mm] # See https://ctan.org/pkg/geometry for more options
header-includes:
  - \linepenalty=10
  - \interlinepenalty=0 # value of the penalty (node) added after each line of a paragraph.
  - \hyphenpenalty=50 # the penalty for line breaking at an automatically inserted hyphen
  - \exhyphenpenalty=50 # the penalty for line breaking at an explicit hyphen
  - \binoppenalty=700 # the penalty for breaking a line at a binary operator
  - \relpenalty=500 # the penalty for breaking a line at a relation
  - \clubpenalty=150 # extra penalty for breaking after first line of a paragraph
  - \widowpenalty=150 # extra penalty for breaking before last line of a paragraph
  - \displaywidowpenalty=50 # extra penalty for breaking before last line before a display math
  - \brokenpenalty=100 # extra penalty for page breaking after a hyphenated line
  - \predisplaypenalty=10000 # penalty for breaking before a display
  - \postdisplaypenalty=0 # penalty for breaking after a display
  - \floatingpenalty = 20000 # penalty for splitting an insertion (can only be split footnote in standard LaTeX)
  - \raggedbottom # or \flushbottom
  - \usepackage{float} # keep figures where there are in the text
  - \floatplacement{figure}{H} # keep figures where there are in the text
---

# Prospects of my scientific career.

My career is closely tied to information technologies and computer science.
Almost every day computing power is growing alongside with the load on it.
Users are no longer willing to wait for several minutes for a web page to 
load or to wait longer than usual for a response from their favorite
application.
Algorithms that were invented years ago are not always up to task.
Thus, it is necessary to improve existing algorithms, conduct research to
find new approaches and develop new ways to solve different type of problems.
So, for example, one of the noticeable evolutionary moments is the widespread
of artificial neural networks.
The technology itself is several decades old.
However, in its original form, it would not cope with a lot of tasks that
are solved today.
Researchers are developing new networks architectures that would be suitable
for different types of tasks.
Thus, on network has state of the art performance in image analysis tasks
(convolutional networks), another one has the best performance in natural
language processing tasks (recurrent), a third one is good for different
types of predictions, etc.
This researches are proving that computer science and information technologies
do not still stand and are constantly evolving.
In order to succeed in this field, it is necessary to evolve along with it.
There are a lot of areas in computer science where a researcher can conduct
researches, such as: artificial intelligence, software verification, algorithms,
etc.
