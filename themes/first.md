---
lang: en-US
title: "Foreign language. My scientific interests and my scientific activities. My department."
author: "Arseny Zorin"

fontsize: 12pt
linestretch: 1.5
documentclass: article
classoption: [notitlepage, onecolumn, openany]
geometry: [a4paper, bindingoffset=0mm, inner=30mm, outer=30mm, top=30mm, bottom=30mm] # See https://ctan.org/pkg/geometry for more options
header-includes:
  - \linepenalty=10
  - \interlinepenalty=0 # value of the penalty (node) added after each line of a paragraph.
  - \hyphenpenalty=50 # the penalty for line breaking at an automatically inserted hyphen
  - \exhyphenpenalty=50 # the penalty for line breaking at an explicit hyphen
  - \binoppenalty=700 # the penalty for breaking a line at a binary operator
  - \relpenalty=500 # the penalty for breaking a line at a relation
  - \clubpenalty=150 # extra penalty for breaking after first line of a paragraph
  - \widowpenalty=150 # extra penalty for breaking before last line of a paragraph
  - \displaywidowpenalty=50 # extra penalty for breaking before last line before a display math
  - \brokenpenalty=100 # extra penalty for page breaking after a hyphenated line
  - \predisplaypenalty=10000 # penalty for breaking before a display
  - \postdisplaypenalty=0 # penalty for breaking after a display
  - \floatingpenalty = 20000 # penalty for splitting an insertion (can only be split footnote in standard LaTeX)
  - \raggedbottom # or \flushbottom
  - \usepackage{float} # keep figures where there are in the text
  - \floatplacement{figure}{H} # keep figures where there are in the text
---

My main scientific interest is the software clone detection.
The existence of clones leads to such problems as unintended increase of
program size that leads to increasing of software maintenance effort.
Fault propagation is another drawback of code clones as duplicating of a
fragment with fault spreads this fault over the project.
On the other hand, there is another perspective on clones, like faster
software development.
The most popular case of code clones appearances is the copy-paste
behaviour of developers.
There are also another reasons why clones occurs, for example, different
developers can implement the same functionality.
Different drawbacks of programming language can also lead to clones 
appearance in a project.
My future PhD thesis will be focused on the detection of semantic
software clones with usage of artificial neural network.
Semantic clones are similar fragments of code that share the same
functionality but differ syntactically.
Detection of this clone type is important from a software maintenance 
point of view.
Clone detection by semantic similarity is undecidable problem in general, as
it is impossible to say whether two program fragments behave equivalently
or not.
Nevertheless, there are several researches that proposes approaches to semantic
software clone detection.
Some of these researches are proposing to detect such clones based on different
types of software testing.
Other researches propose to analyze binary code for detecting such types
of software clones.
The main goal of my research is to find out the possibility of semantic code
clones detection with help of neural network algorithms.
The other one is to develop a prototype of a tool which will use such approach.
At the moment my main activity in such research is writing review articles
in which I am providing comparison of different approaches of detecting
not only semantic clones but also syntactic ones.
